Add in ~/.bashrc

# Set the model path so Gazebo finds the models
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:$FUSEON_STACK/packages/gazebo/fuseon_gazebo/models
export GAZEBO_MODEL_PATH=${GAZEBO_MODEL_PATH}:$FUSEON_STACK/packages/gazebo/fuseon_gazebo/models/aruco_visual_markers
